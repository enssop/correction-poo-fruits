<?php

require 'Panier.php';
require 'Fruit.php';

$panier = new Panier();
$panier->ajouterFruit(new Fruit("pomme", "rouge", 100));
$panier->ajouterFruit(new Fruit("banane", "jaune", 150));
echo $panier->calculerPoids() . "\n";
