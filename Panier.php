<?php

class Panier
{

    private array $contenu;

    public function ajouterFruit(Fruit $fruit): void
    {
        $this->contenu[] = $fruit;
    }

    public function calculerPoids(): int
    {
        $p = 0;
        foreach ($this->contenu as $fruit) {
            $p += $fruit->getPoids();
        }
        return $p;
    }
}
