<?php

class Fruit
{
    private string $type;
    private string $couleur;
    private int $poids;

    public function __construct(string $type, string $couleur, int $poids)
    {
        $this->type = $type;
        $this->couleur = $couleur;
        $this->poids = $poids;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType($type)
    {
        if (in_array($type, ['pomme', 'banane', 'fraise'])) {
            $this->type = $type;
        }
    }

    public function getCouleur()
    {
        return $this->couleur;
    }

    public function getPoids()
    {
        return $this->poids;
    }


}
